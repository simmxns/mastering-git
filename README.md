# 25 days of git (Mastering git)
The easy docs to know everything about git source control
<div align="center" style="display:flex;">
  <img src="https://www.andrehatlo.com/assets/images/git-banner.png" />
  <i>Git stuff</i>
</div>

## Table of contents
* [What is git](#what-is-git)
* [What you goung to get](#what-you-going-to-get)
* [Getting started](#getting-started)
* [Contributing](#contributing)
* [Project status](#project-status)

## What is git?
It is an open source project that gives you the possibility to manage versions in your projects, which means that you can work in the same directory but at different times in it, it is like managing the time of your projects, you can be sure to make changes git will take care of tracking everything that happens in its directory and will store it in its history so that all changes in your repository are recorded, if you want to continue making changes, git will always be aware of what is deleted, modified, created, etc...

## What you going to get?
Actually this is a repo of my git knowledge and learning but, just that I wanted to turn it in 25 leasons (1 per day) to teach the people how to properly use git and make known every single git trick that I know. 

So you going to get all the basics, and advanced about git, create a repo, track files, track modified files, make good commits, delete files, push files, undo push, etc...

The repository will be sectioned into days, the goal is to finish a tranche in one day. I am going to try to make it as interactive as possible to make it as comfortable as possible to understand and practice.

On each day you will get a readme file and some files to edit or maybe you will be the one to create those files, this way you can learn the theory and practice in just one day.

I know to much text, but i wanted to give you a quick look of the project.

With nothing more to say let's get to it!

## Getting started
```sh
# just
git clone https://gitlab.com/simmxns/mastering-git.git
cd mastering-git
# happy coding!
```

## Contributing
This repo currently is in developemnt, and will be in constant maintenance, so feel free to colaborate on it, thx 🤠

## Project status
> In development
> 25/05/2022
