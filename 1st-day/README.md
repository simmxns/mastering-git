# Start now (first day)

## Config a repo
### `git init, clone y config`

## What is a git repo?
Is a virtual storage of your project. allows you to save versions of your code, that you can access later to see those changes

## Start a git repo `git init`
`git init` will create the .git folder in the current or selected directory

### Bares
```sh

```

### Templates
```sh
git init <directory> --template=<template_directory>
```